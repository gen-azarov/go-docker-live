# Go-Docker-Live

This is a websocket service to get notifications and live display of god.log file on Go-Docker running tasks.

While a job is running, user can view, with go-d-live, the god.log (standard output) of a running job.
He can also receive live notifications of job termination.


testpubsub.js are only test/example files to submit some notifications to a user.

# Docker

Service listens on port 8001, and config file is in /root/config/default.json

    docker run  -v ${PWD}/config/default.json:/root/config/default.json -p 8001:8001 osallou/go-docker-live

# Manual installation

Install nodejs then

npm install

# Configuration

configuration is available in config/default.json

Redis configuration must match the redis server used by GoDocker.

# Running

node godlive.js

As a service:

node service.js

# Proxying

To proxy requests to go-docker-live, a web proxy must forward web sockets (nginx or apache >= 2.4). Proxy should act as an HTTPS endpoint forwarding to HTTP.

Example for nginx:


    server {

        listen       8001 ssl;

        server_name  godocker.myserver.com;

        ssl_certificate     server.crt;

        ssl_certificate_key server.key;

        ssl_protocols       TLSv1 TLSv1.1 TLSv1.2;

        ssl_ciphers         HIGH:!aNULL:!MD5;


        location / {

            proxy_pass http://godockerlive:8001;

            proxy_set_header X-Real-IP $remote_addr;

            proxy_set_header Host $http_host;

            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;

        }

    }

Example for apache in SSL virtualhost:

        
		... SSL certificate definition etc.
		...
        ProxyPreserveHost on

        # Proxy web sockets to godockerlive
        RewriteEngine on
        RewriteCond %{QUERY_STRING} transport=polling
        RewriteRule /(.*)$ http://godockerlive:8001/$1 [P]

        ProxyRequests off
        ProxyPass /socket.io ws://godockerlive:8001/socket.io
        ProxyPassReverse /socket.io ws://godockerlive:8001/socket.io

        # Proxy web server to godocker web server
        ProxyPass         / http://godockerweb/
        ProxyPassReverse  / http://godockerweb/
